#!/bin/bash

echo "Copying custom header"
cp /etc/georchestra/header/customizations/index.jsp /var/lib/jetty/webapps/header/WEB-INF/jsp/
I18N_DIR=/var/lib/jetty/webapps/header/WEB-INF/classes/_header/i18n/
for f in `ls -1 $I18N_DIR`; do
  echo "datahub=datahub" >> $I18N_DIR/$f
done